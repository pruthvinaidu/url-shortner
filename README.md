
	The functionality of this is to create a short url of the given url. 
	Once user click on the short url then it will take user to the desired page.
	With this feature we can hide the big urls and share the short url to end user.
	
	master branch contains the backend part.
    source branch contains the frontend part.
	
	The frond end part was developed by using Angular
    In src file a component with name short was created 
	the Short component has the both html and typeScript code
	the modules added are ReactivFormsModule and HttpClientModule
	we can run it in localhost:4200
	
	The Backend part was developed by using Spring Tool suite4
	The Project developed as Spring Starter project which has diiferent class 
	The UrlController class was to map to the service class
	The UsrlService class has Contains the required logic to develop
	The UrlRepository class was to manage the data, conatins the logic that required to access data source 
	The UrlEntity class the helps us to maps to table in data base
	The AppConstant interface contains the variables that are intialized